// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package cmdr

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"runtime/debug"
	"strings"

	"gitlab.com/zygoon/go-cmdr/internal"
)

// Args0Observer instructs RunMain to preserve args[0].
type Args0Observer interface {
	PreserveArgs0() bool
}

// ExitCoder is an interface for errors to convey a process exit code.
type ExitCoder interface {
	ExitCode() int
}

// ExitMessenger is an interface for errors to convey a specific error message.
//
// ExitMessenger may be used to suppress the system that automatically prints
// the error value if the error message returned is empty.
type ExitMessenger interface {
	ExitMessage() string
}

// SilentError is an error type that produces a given error code but no error message.
type SilentError uint8

// Error returns the empty string.
func (e SilentError) Error() string {
	return ""
}

// ExitMessage returns the empty string.
//
// cmdr.RunMain suppresses error output when ExitMessage is implemented by the
// error value and the value is the empty string.
func (e SilentError) ExitMessage() string {
	return ""
}

// ExitCode returns the integer encoded in the error.
func (e SilentError) ExitCode() int {
	return int(e)
}

// PanicBehavior controls reaction to panic encountered in RunMain.
//
// Use WithPanicBehavior to
type PanicBehavior int

const (
	// RecoverExit is the default panic behavior of RunMain.
	//
	// Panic caused by command execution is recovered. The recovered value
	// is printed to standard error. Program execution is terminated with
	// os.Exit, or the configured exit function, with code 70.
	RecoverExit PanicBehavior = iota

	// RecoverTraceExit prints a stack trace on panic.
	//
	// RecoverStackTrace is like RecoverExit in the sense that a panic is
	// both recovered and that the process terminates with an exit code. In
	// addition, the stack trace of the go routine which has caused the
	// panic is printed to standard error.
	RecoverTraceExit
)

type mainOpts struct {
	argv0 string
	sigs  []os.Signal
	ctx   context.Context

	panicBehavior PanicBehavior
	exitFunc      func(int)
}

// Option influences how RunMain operates.
type Option func(*mainOpts)

// WithSignals sets signals canceling the context passed to the main command.
//
// RunMain establishes signal handlers through signal.NotifyContext. By default,
// os.Interrupt is intercepted. This option allows using a different set of
// signals.
func WithSignals(sigs ...os.Signal) Option {
	return func(m *mainOpts) {
		m.sigs = sigs
	}
}

// WithArgv0 sets the name of the main command.
//
// RunMain occasionally prints the name of the main command. The name of the
// main command is either "?" or the base name of os.Args[0]. This option
// allows using a custom name.
func WithArgv0(argv0 string) Option {
	return func(m *mainOpts) {
		m.argv0 = argv0
	}
}

// WithBaseContext sets the base of the context provided to the main command.
//
// RunMain provides a context to executed command. This context defaults to a
// context derived from context.Background. This option allows using a custom
// base context, for example, a context with a deadline.
func WithBaseContext(ctx context.Context) Option {
	return func(m *mainOpts) {
		m.ctx = ctx
	}
}

// WithPanicBehavior configures panic behavior.
//
// RunMain recovers any panic that happens during execution of the main
// command. By default, only the panic message is printed. This option allows
// selecting other behavior.
func WithPanicBehavior(b PanicBehavior) Option {
	return func(m *mainOpts) {
		m.panicBehavior = b
	}
}

// WithExitFunc overrides the default os.Exit with a custom function.
//
// Note that the exit function when a non-zero needs to be returned. On
// successful exit or when the error maps to an exit code of zero, the exit
// function is not called.
func WithExitFunc(fn func(code int)) Option {
	return func(m *mainOpts) {
		m.exitFunc = fn
	}
}

// RunMain runs the given command with arguments.
//
// A context.NotifyContext with os.Interrupt signal is provided as context,
// allowing commands to gracefully terminate on the interrupt signal.
// Other signals may be intercepted by passing WithSignals option.
//
// If cmd has PassArgv0() bool method which returns true, then args[0] is
// preserved. This allows implementing symlink-based command multiplexers,
// such as Router.
//
// If the command panics, the panic is recovered and an error message is
// printed. Other behaviors can be selected by passing WithPanicBehavior
// option. If the command returns an error, the error is printed, unless
// it is equal to flag.ErrHelp.
//
// The exit code from the process is non-zero when the command returns an
// error or causes a panic. Errors implementing the ExitCoder interface
// can provide the exit code explicitly. The special flag.ErrHelp
// is translated to EX_USAGE (64). All other errors set exit code to one.
//
// The standard output and error streams are always synchronized before
// terminating the process.
func RunMain(cmd Cmd, args []string, opts ...Option) {
	m := mainOpts{
		argv0:    "?",
		sigs:     []os.Signal{os.Interrupt},
		ctx:      context.Background(),
		exitFunc: os.Exit,
	}

	if len(args) > 0 {
		m.argv0 = args[0]
	}

	for _, opt := range opts {
		opt(&m)
	}

	m.argv0 = strings.TrimSuffix(filepath.Base(m.argv0), ExeSuffix)

	var rc int

	_, stdout, stderr := Stdio(m.ctx)

	defer func() {
		if r := recover(); r != nil {
			_, _ = fmt.Fprintf(stderr, "%s panic: %v\n", m.argv0, r)

			if m.panicBehavior == RecoverTraceExit {
				debug.PrintStack()
			}

			rc = 70 // EX_SOFTWARE
		}

		// Ensure that standard IO streams are synchronized before calling the exit
		// function which may abruptly terminate the process.
		if stdout, ok := stdout.(*os.File); ok {
			_ = stdout.Sync()
		}

		if stderr, ok := stderr.(*os.File); ok {
			_ = stderr.Sync()
		}

		if rc != 0 {
			// Exit the process with the desired return code. This is done in an
			// indirect way since os.Exit terminates the process and doesn't let
			// the runtime run deferred calls.
			m.exitFunc(rc)
		}
	}()

	ctx, stop := signal.NotifyContext(m.ctx, m.sigs...)
	defer stop()

	ctx = internal.WithName(ctx, m.argv0)
	ctx = internal.WithArgs(ctx, args)

	// Strip args[0] unless the command expects to see it.
	if cmd, ok := cmd.(Args0Observer); (!ok || !cmd.PreserveArgs0()) && len(args) > 0 {
		args = args[1:]
	}

	err := cmd.Run(ctx, args)
	if err == nil {
		return
	}

	// Allow errors to carry an explicit error code.
	if coder, ok := err.(ExitCoder); ok {
		rc = coder.ExitCode()
	} else if errors.Is(err, flag.ErrHelp) {
		rc = 64 // EX_USAGE
	} else {
		rc = 1
	}

	var exitMsg string

	if messenger, ok := err.(ExitMessenger); ok {
		exitMsg = messenger.ExitMessage()
	} else if errors.Is(err, flag.ErrHelp) {
		// flag.ErrHelp does not produce an error message.
	} else {
		exitMsg = fmt.Sprintf("%s error: %s", m.argv0, err)
	}

	if exitMsg != "" {
		_, _ = fmt.Fprintln(stderr, exitMsg)
	}
}

// RunSubCommand runs a given command as a sub-command.
//
// This function allows replicating all the router functionality in a custom
// type, perhaps one with logic beyond the capabilities of the default router.
//
// Unlike just running the command directly, the invoked sub-command can access
// the correctly nested sub-command name through cmdr.Name.
func RunSubCommand(ctx context.Context, cmd Cmd, name string, args []string) error {
	ctx = internal.WithName(ctx, internal.Name(ctx)+" "+name)

	return cmd.Run(ctx, args)
}

// Name returns the name of the command.
//
// Name always returns the accurate name of the executing command, which may be
// a nested name, for as long as RunMain and router.Cmd are used.
//
// If the context is handled incorrectly, Name panics.
func Name(ctx context.Context) (name string) {
	return internal.Name(ctx)
}
