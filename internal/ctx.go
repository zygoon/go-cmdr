// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package internal

import (
	"context"
)

type nameKey struct{}

// WithName returns a context remembering the command name.
func WithName(parent context.Context, name string) context.Context {
	return context.WithValue(parent, nameKey{}, name)
}

// Name returns the name of currently executing command.
func Name(ctx context.Context) (name string) {
	if name, ok := ctx.Value(nameKey{}).(string); ok {
		return name
	}

	panic("context misuse, command name not set")
}

type argsKey struct{}

// WithArgs returns a context remembering the argument slice.
func WithArgs(parent context.Context, args []string) context.Context {
	return context.WithValue(parent, argsKey{}, args)
}

// Args returns the entire argument slice of a command.
func Args(ctx context.Context) (args []string) {
	if args, ok := ctx.Value(argsKey{}).([]string); ok {
		return args
	}

	panic("context misuse, argument slice not set")
}
