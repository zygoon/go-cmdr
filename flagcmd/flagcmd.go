// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// Package flagcmd implements extensible commands based on standard library
// flag.FlagSet argument parser.
package flagcmd

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
)

// Cmd is a command that is also a flag runner.
type Cmd interface {
	cmdr.Cmd
	FlagRunner
}

// FlagRunner wraps the RunFlag and FlagSet methods.
//
// A flag runner is similar to a cmdr.Cmd in that it can be invoked, but differs
// that it needs an external entity to parse command line arguments.
type FlagRunner interface {
	// RunFlag runs the command with the given context and parsed flag set.
	RunFlag(ctx context.Context, f *flag.FlagSet) error
	// FlagSet returns a new, configured FlagSet.
	FlagSet(ctx context.Context) *flag.FlagSet
}

// FlagExtender allows extending the flag set of a base.
type FlagExtender interface {
	// ExtendFlag extends an existing FlagSet and context.
	ExtendFlag(ctx context.Context, fl *flag.FlagSet) context.Context
	// RunFlag runs the extender given a context and parsed flag set.
	RunFlag(ctx context.Context, fl *flag.FlagSet) error
}

// Helper is the interface that wraps the FlagUsage function.
type Helper interface {
	// FlagUsage is the function usable for flag.FlagSet.Usage
	// Unlike the original, it is given the flag set it relates to.
	FlagUsage(fl *flag.FlagSet)
}

// MakeCmd returns a command combining a flag runner with extensions.
//
// The resulting command uses the runner to construct a flag set and each
// extension to extend it. Once flags are parsed, each extension is given an
// option to act on the values. Lastly, the runner itself is invoked.
//
// If r implements Helper, then the flag set it returns is automatically
// configured for usage instructions by setting FlagSet.Usage.
//
// MakeCmd panics if any of the extenders returns a nil context.
func MakeCmd(r FlagRunner, es ...FlagExtender) cmdr.Cmd {
	return cmdr.Func(func(ctx context.Context, args []string) error {
		_, _, stderr := cmdr.Stdio(ctx)

		fl := r.FlagSet(ctx)
		fl.SetOutput(stderr)

		if c, ok := r.(Helper); ok {
			fl.Usage = func() { c.FlagUsage(fl) }
		}

		for _, e := range es {
			ctx = e.ExtendFlag(ctx, fl)
			if ctx == nil {
				panic(fmt.Sprintf("%T.ExtendFlag returned nil context", e))
			}
		}

		if err := fl.Parse(args); err != nil {
			return err
		}

		for _, e := range es {
			if err := e.RunFlag(ctx, fl); err != nil {
				return err
			}
		}

		return r.RunFlag(ctx, fl)
	})
}
