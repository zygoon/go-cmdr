// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package flagcmd_test

import (
	"context"
	"flag"
	"fmt"
	"testing"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/cmdtest"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
)

type cmdUsage struct {
	usageFn func(*flag.FlagSet)
}

func (cmdUsage) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (cmdUsage) RunFlag(context.Context, *flag.FlagSet) error {
	return nil
}

func (cmd cmdUsage) FlagUsage(fl *flag.FlagSet) {
	if cmd.usageFn != nil {
		cmd.usageFn(fl)
	}
}

func (cmd cmdUsage) Run(ctx context.Context, args []string) error {
	ext := flagcmd.ExtensionFunc(func(ctx context.Context, fl *flag.FlagSet) context.Context {
		fl.Bool("dummy", false, "Dummy option")

		return ctx
	})

	return flagcmd.MakeCmd(cmd, ext).Run(ctx, args)
}

func TestMakeCmdHelp(t *testing.T) {
	t.Run("usage-noop", func(t *testing.T) {
		inv := cmdtest.Invoke(cmdUsage{}, "--help")

		if err := inv.ExpectStdout(""); err != nil {
			t.Error(err)
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Error(err)
		}

		if err := inv.ExpectExitCode(64); err != nil {
			t.Fatal(err)
		}
	})

	t.Run("usage-print-defaults", func(t *testing.T) {
		inv := cmdtest.Invoke(cmdUsage{usageFn: func(fl *flag.FlagSet) { fl.PrintDefaults() }}, "--help")

		if err := inv.ExpectStdout(""); err != nil {
			t.Error(err)
		}

		const expectedHelp = "  -dummy\n    <TAB>Dummy option\n"
		if err := inv.ExpectStderr(expectedHelp); err != nil {
			t.Error(err)
		}

		if err := inv.ExpectExitCode(64); err != nil {
			t.Fatal(err)
		}
	})

	t.Run("usage-custom", func(t *testing.T) {
		inv := cmdtest.Invoke(cmdUsage{usageFn: func(fl *flag.FlagSet) { _, _ = fmt.Fprintln(fl.Output(), "Custom usage message") }}, "--help")

		if err := inv.ExpectStdout(""); err != nil {
			t.Error(err)
		}

		if err := inv.ExpectStderr("Custom usage message\n"); err != nil {
			t.Error(err)
		}

		if err := inv.ExpectExitCode(64); err != nil {
			t.Fatal(err)
		}
	})
}
