// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package flagcmd_test

import (
	"context"
	"flag"
	"fmt"
	"testing"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/cmdtest"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
)

func TestRunnerFunc(t *testing.T) {
	r := flagcmd.RunnerFunc(func(ctx context.Context, fl *flag.FlagSet) error {
		_, stdout, _ := cmdr.Stdio(ctx)

		_, _ = fmt.Fprintln(stdout, "Hello World")

		return nil
	})

	cmd := flagcmd.MakeCmd(r)
	inv := cmdtest.Invoke(cmd)

	if err := inv.ExpectStdout("Hello World\n"); err != nil {
		t.Error(err)
	}

	if err := inv.ExpectStderr(""); err != nil {
		t.Error(err)
	}

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}
}

func TestRunnerFuncExtensionFunc(t *testing.T) {
	type keyType struct{}

	r := flagcmd.RunnerFunc(func(ctx context.Context, fl *flag.FlagSet) error {
		_, stdout, _ := cmdr.Stdio(ctx)

		if valPtr := ctx.Value(keyType{}).(*bool); *valPtr {
			_, _ = fmt.Fprintln(stdout, "The flag was passed")
		}

		return nil
	})

	es := flagcmd.ExtensionFunc(func(ctx context.Context, fl *flag.FlagSet) context.Context {
		flagPtr := fl.Bool("flag", false, "Some flag")

		return context.WithValue(ctx, keyType{}, flagPtr)
	})

	cmd := flagcmd.MakeCmd(r, es)
	inv := cmdtest.Invoke(cmd, "-flag")

	if err := inv.ExpectStdout("The flag was passed\n"); err != nil {
		t.Error(err)
	}

	if err := inv.ExpectStderr(""); err != nil {
		t.Error(err)
	}

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}
}
