// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package flagcmd

import (
	"context"
	"flag"
)

// ExtensionFunc adapts a Go function to act as a FlagExtender.
type ExtensionFunc func(ctx context.Context, fl *flag.FlagSet) context.Context

// RunFlag does nothing.
func (ExtensionFunc) RunFlag(context.Context, *flag.FlagSet) error {
	return nil
}

// ExtendFlag runs the underlying function.
func (fn ExtensionFunc) ExtendFlag(ctx context.Context, fl *flag.FlagSet) context.Context {
	return fn(ctx, fl)
}
