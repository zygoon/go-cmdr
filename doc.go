// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package cmdr provides an organized way to implement command line tools, with
// predictable behavior, arbitrary sub-commands and symlink-based multiplexing.
package cmdr
