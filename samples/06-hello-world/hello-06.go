// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"context"
	"flag"
	"fmt"
	"os"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
)

// Before we move to another topic let's add one last part to the hello-world
// program - tests! Testing your command line logic is just as important as testing
// any other part of your program. It takes discipline, tooling and some care in
// designing things to be testing-friendly.

type mainCmd struct {
	lang string
}

func (cmd *mainCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	fl := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fl.StringVar(&cmd.lang, "l", "en", "Greeting Language (en, pl)")

	return fl
}

func (cmd *mainCmd) RunFlag(ctx context.Context, _ *flag.FlagSet) error {
	// The Stdio function returns the tree I/O streams: standard input (stdin),
	// standard output (stdout) and standard error (stderr). Unlike their global
	// counterparts in the os package, they are easily to replace and may be
	// replaced with any tuple of (io.Reader, io.Writer, io.Writer).
	//
	// Doing the replacement this way is less fragile and more convenient than
	// altering the global variables.
	_, stdout, _ := cmdr.Stdio(ctx)

	switch cmd.lang {
	case "", "en":
		// The only caveat is that we now use Fprintf instead of plain Printf.
		// It _is_ easy to miss this, so some rigor is required. Approaches that
		// exercise the code and run system-level replacements are possible on
		// certain platforms, but they were deemed too heavy-handed to provide a
		// nice experience.
		_, _ = fmt.Fprintf(stdout, "Hello World\n")
	case "pl":
		// spell-checker: disable-next-line
		_, _ = fmt.Fprintf(stdout, "Witaj świecie\n")
	}

	// By the way, this program contains a bug. Run the tests to find it.
	return nil
}

func (cmd *mainCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func main() {
	cmdr.RunMain(&mainCmd{}, os.Args)
}
