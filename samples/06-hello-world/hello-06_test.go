// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main_test

import (
	"testing"

	"gitlab.com/zygoon/go-cmdr/cmdtest"

	main "gitlab.com/zygoon/go-cmdr/samples/06-hello-world"
)

func TestMainCmd(t *testing.T) {
	t.Run("no-args", func(t *testing.T) {
		// The cmdtest package comes with several test utilities. Chief among them is
		// the Invoke function with given a command (and any number of positional
		// arguments), returns an object that captures the result of the invocation.
		inv := cmdtest.Invoke(&main.Cmd{})
		// The ExpectStdout, ExpectStderr and ExpectExitCode helpers return errors
		// that contain useful information about the failure. Notably the first
		// contain a fair amount of quality-of-life improvements for working with
		// program output.
		//
		// Build a more complex program, put tabs, spaces, trailing spaces and play
		// around with this test. To find out more about how this works look at:
		//
		// - go doc gitlab.com/zygoon/go-cmdr/cmdtest.CannedOutput
		// - go doc gitlab.com/zygoon/go-cmdr/cmdtest.CannedOutput.MatchesBuffer
		//
		// Those are all used internally by ExpectStdout and ExpectStderr, but
		// knowing how they work will allow you to write your tests more clearly.
		if err := inv.ExpectStdout("Hello World\n"); err != nil {
			// Log the error but allow the test to continue, as there may be some clue
			// as to what is wrong on standard error and we want to see that.
			t.Error(err)
		}

		if err := inv.ExpectStderr(""); err != nil {
			// Log the error and carry on a little more so that we get to check the
			// exit code.
			t.Error(err)
		}

		if err := inv.ExpectExitCode(0); err != nil {
			// The last of the checks uses t.Fatal to stop test execution if the exit
			// code is differs from the expected value.
			t.Fatal(err)
		}
	})

	t.Run("English", func(t *testing.T) {
		inv := cmdtest.Invoke(&main.Cmd{}, "-l", "en")
		if err := inv.ExpectStdout("Hello World\n"); err != nil {
			t.Error(err)
		}
	})

	t.Run("Polish", func(t *testing.T) {
		inv := cmdtest.Invoke(&main.Cmd{}, "-l", "pl")
		if err := inv.ExpectStdout("Witaj świecie\n"); err != nil {
			t.Error(err)
		}
	})

	t.Run("German", func(t *testing.T) {
		inv := cmdtest.Invoke(&main.Cmd{}, "-l", "de")
		// Oops, we found a bug in the original program!
		if err := inv.ExpectStdout("Hallo Welt\n"); err != nil {
			t.Log(err)
			// Uncomment the next line to see the problem:
			// t.Fail()
		}
	})
}
