// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

// It's good practice to separate public and private APIs for clarity and
// stability. The idiomatic "export_test.go" file allows us to export the
// mainCmd type to tests as the public Cmd type.
type Cmd = mainCmd
