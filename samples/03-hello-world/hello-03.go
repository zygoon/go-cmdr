// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"context"
	"flag"
	"fmt"
	"os"

	"gitlab.com/zygoon/go-cmdr"
)

type mainCmd struct{}

func (mainCmd) Run(_ context.Context, args []string) error {
	// The previous example merely printed the hello-world message. Realistic
	// programs handle command-line options, display help and act on arguments.
	//
	// Commander is a thin framework, so it doesn't try to do everything or
	// prescribe one way of doing something. You can use the standard library
	// "flag" package to handle basic command line arguments.
	//
	// Regardless if you've seen the flag package before, or if this is your first
	// encounter, the code below is almost self-explanatory. The program creates
	// a flag set, adds a string argument "-l", with the default value "en" and
	// a help message, parses the arguments given to the Run function, handles
	// any error encountered and then proceeds to display a message in the
	// selected language.
	//
	// Try running this program in several ways:
	// - without _any_ argument
	// - with the argument: --help
	// - with two arguments: -l pl
	// - with the argument: -invalid
	//
	// Observe what happens at runtime and what the exit status is.
	var lang string

	fl := flag.NewFlagSet("03-hello-world", flag.ContinueOnError)
	fl.StringVar(&lang, "l", "en", "Greeting Language (en, pl)")

	if err := fl.Parse(args); err != nil {
		// This code can return with an error value. Commander has a number of
		// available behaviors for working with errors (and panics). Look at
		// the documentation of the following symbols:
		//
		//  - go doc gitlab.com/zygoon/go-cmdr.RunMain
		//  - go doc gitlab.com/zygoon/go-cmdr.ExitCoder
		//  - go doc gitlab.com/zygoon/go-cmdr.ExitMessenger
		//
		// There's one bit of special-case, the flag.ErrHelp error is treated
		// as SilentError(64), where the exit code 64 is based on the UNIX C
		// macro EX_USAGE.
		//
		// The intent is to make the default behavior for most errors useful,
		// while allowing specific behavior to be possible with the help of a
		// few extra functions that implement the interfaces mentioned earlier.
		return err
	}

	switch lang {
	case "", "en":
		_, _ = fmt.Printf("Hello World\n")
	case "pl":
		// spell-checker: disable-next-line
		_, _ = fmt.Printf("Witaj świecie\n")
	}

	return nil
}

func main() {
	cmdr.RunMain(mainCmd{}, os.Args)
}
