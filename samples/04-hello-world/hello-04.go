// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"context"
	"flag"
	"fmt"
	"os"

	"gitlab.com/zygoon/go-cmdr"
)

type mainCmd struct{}

func (mainCmd) Run(ctx context.Context, args []string) error {
	var lang string

	// The cmdr package comes with several convenience functions. One of them
	// is cmdr.Name - a function that takes a context and returns the name of
	// the command being invoked.
	//
	// In simple commands it may seem trivial, but as complexity grows, code is
	// shared across modules or is used within sub-commands, this makes it
	// easier to just do the right thing.
	fl := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fl.StringVar(&lang, "l", "en", "Greeting Language (en, pl)")

	if err := fl.Parse(args); err != nil {
		return err
	}

	switch lang {
	case "", "en":
		_, _ = fmt.Printf("Hello World\n")
	case "pl":
		// spell-checker: disable-next-line
		_, _ = fmt.Printf("Witaj świecie\n")
	}

	return nil
}

func main() {
	cmdr.RunMain(mainCmd{}, os.Args)
}
