// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
)

// Let's move the language selection feature out from the main command and into
// an extension that we can then share across commands. This allows building reusable
// blocks and composing them into libraries.

type LanguageExt struct {
	lang      string
	Supported []string
}

// Lang returns the selected language.
func (ext *LanguageExt) Lang() string {
	return ext.lang
}

func (ext *LanguageExt) ExtendFlag(ctx context.Context, fl *flag.FlagSet) context.Context {
	// ExtendFlag allows extensions to add command line options to a flag set.
	// The extension also gets access to the execution context and may modify
	// it, for example by using context.WithValue. In this extension we expect
	// the users to embed it directly (so they have access to the instance) so
	// we just return the same context.
	fl.StringVar(&ext.lang, "l", "en", fmt.Sprintf("Greeting Language (%s)", strings.Join(ext.Supported, ", ")))

	return ctx
}

func (ext *LanguageExt) RunFlag(context.Context, *flag.FlagSet) error {
	// RunFlag is the same method that FlagRunner provides. Each extension gets
	// a chance to run after the command line arguments have been parsed. This
	// can be used to act on the values provided by the user. You can set up
	// logging, enable debugging, connect to a remote service, open a file or
	// do whatever makes sense in your program.
	for _, l := range ext.Supported {
		if l == ext.lang {
			return nil
		}
	}

	// Unlike the earlier iteration of this example, we only support certain
	// languages and refuse to start if the selected language is not available
	// in the program.
	return fmt.Errorf("unsupported language: %s", ext.lang)
}

type mainCmd struct {
	// We can directly embed the extension in the command. This does mean we
	// now have to pass the command by pointer to avoid copying the state over
	// and over and to allow the state to be mutated.
	LanguageExt
}

func (cmd *mainCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (cmd *mainCmd) RunFlag(ctx context.Context, _ *flag.FlagSet) error {
	_, stdout, _ := cmdr.Stdio(ctx)

	// We can access the language directly. In cases where the state of the
	// extension is provided through the context we would instead call a
	// function that loads the value from the context provided to the function.
	switch cmd.LanguageExt.Lang() {
	case "", "en":
		_, _ = fmt.Fprintf(stdout, "Hello World\n")
	case "pl":
		// spell-checker: disable-next-line
		_, _ = fmt.Fprintf(stdout, "Witaj świecie\n")
	}

	return nil
}

func (cmd *mainCmd) Run(ctx context.Context, args []string) error {
	cmd.LanguageExt.Supported = []string{"en", "pl"}

	return flagcmd.MakeCmd(cmd, &cmd.LanguageExt).Run(ctx, args)
}

func main() {
	cmdr.RunMain(&mainCmd{}, os.Args)
}
