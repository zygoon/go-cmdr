<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Zygmunt Krynicki
-->

# Sample Programs

This directory contains a separate Go module with sample programs that showcase
some of the ideas behind, and features of, the commander module. They are meant
to be read in order similar to lessons in a tutorial.

Consult the [go-cmdr reference](https://pkg.go.dev/gitlab.com/zygoon/go-cmdr) for
many topics not yet covered here.

## Feedback is welcome!

Please provide feedback by [opening issues](https://gitlab.com/zygoon/go-cmdr/-/issues/new).

## Program Index

This index may help to find the concept described by the particular sample
program. At the moment a sample "Hello World" program is used to introduce
several concepts of the library.

### Hello World

- 01-hello-world - Minimalistic program using `cmdr.RunMain` and `cmdr.Func`.
- 02-hello-world - Commands as empty structure types, implementing `cmdr.Cmd`.
- 03-hello-world - Parsing command line arguments with `flag.FlagSet`, returning `error` values.
- 04-hello-world - Using `cmdr.Name` for parser name.
- 05-hello-world - Avoiding boilerplate with `flagcmd.MakeCmd` and `flagcmd.FlagRunner`.
- 06-hello-world - Testing programs with `cmdr.Stdio`, `cmdtest.Invoke` and `cmdtest.Invocation.ExpectedStdout`.
- 07-hello-world - Using command extensions with `flagcmd.FlagExteder`.

