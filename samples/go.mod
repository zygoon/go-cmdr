module gitlab.com/zygoon/go-cmdr/samples

go 1.22.2

require gitlab.com/zygoon/go-cmdr v1.9.0

replace gitlab.com/zygoon/go-cmdr => ./..
