// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// ^ The first two lines contain SPDX annotations - a form of machine-readable
// licensing and copyright data that is more and more popular amongst software
// projects. Keeping this in each file makes it easier to keep track of code as
// it moves around projects. You can copy any code from any file in this
// project, just make sure to copy the SPDX headers as well, to know how it
// affects your code.

package main

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/zygoon/go-cmdr"
)

func main() {
	// The utmost basic hello-world program is not too different from the
	// classic Go counterpart. Notable differences include support for
	// returning an error value and for accepting the context argument.
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		_, _ = fmt.Printf("Hello World\n")

		return nil
	})

	// RunMain takes a value of type:
	//
	// interface { Run(ctx context.Context, args []string) error }
	//
	// This interface is known as cmdr.Cmd - the most basic command.
	//
	// Anything with a Run function, taking a context, argument slice and
	// returning an error can be used as a command. The context is canceled
	// when an interrupt signal arrives, allowing your programs to act
	// accordingly and shut down gracefully. Any error you return is nicely
	// formatted and displayed.
	//
	// The go-cmdr module is lightweight and composable. The essence is that
	// you can run things easily, making use of shared components or rolling
	// your own.
	//
	// As you explore the library, you will find many small gems that make
	// writing and testing programs nicer, but you can always choose your own
	// path as things are loosely coupled and replaceable.
	cmdr.RunMain(cmd, os.Args)
}
