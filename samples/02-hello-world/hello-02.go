// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/zygoon/go-cmdr"
)

// The previous example (01-hello-world), used cmdr.Func which wraps a function
// into a runnable command. It is convenient to use the empty structure idiom
// and treat individual commands as distinct types.
//
// The essence does not change - the type still has a Run function, but this
// approach is easier to work with as the complexity of the system grows.
type mainCmd struct{}

func (mainCmd) Run(context.Context, []string) error {
	_, _ = fmt.Printf("Hello World\n")

	return nil
}

func main() {
	cmdr.RunMain(mainCmd{}, os.Args)
}
