// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"context"
	"flag"
	"fmt"
	"os"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
)

// This sample introduces a new package: flagcmd. The idea behind flagcmd is to
// make cmdr work even better with the flag package from the standard library.
// There's generally less boilerplate code to write, more cohesive behavior
// and more ways to reuse code across your programs and libraries (more of that
// will be evident in the upcoming samples).
//
// # The flagcmd.FlagRunner interface
//
// This interface is defined as follows:
//
//	type FlagRunner interface {
//	       RunFlag(ctx context.Context, f *flag.FlagSet) error
//	       FlagSet(ctx context.Context) *flag.FlagSet
//	}
//
// As one can see, the interface separates obtaining a FlagSet from running the
// body of the command after the arguments are parsed. This doesn't quite fit
// the Cmdr interface yet. The rest is filled in by the function flagcmd.MakeCmd,
// which takes a flagcmd.FlagRunner and returns a cmdr.Cmd value.

type mainCmd struct {
	// Notice that the state of the command has now moved from the Run function
	// to the mainCmd type. This allows us to access the values that the flags
	// represent across multiple functions.
	lang string
}

func (cmd *mainCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	// The FlagSet function is responsible for returning a flag.FlagSet that
	// describes all the flags that are required.
	fl := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fl.StringVar(&cmd.lang, "l", "en", "Greeting Language (en, pl)")

	return fl
}

func (cmd *mainCmd) RunFlag(context.Context, *flag.FlagSet) error {
	// The RunFlag function is responsible for running the command after all the
	// command line parsing and error handling has been completed. Note that only
	// options are handled this way, positional arguments are still managed
	// outside the flag package.
	switch cmd.lang {
	case "", "en":
		_, _ = fmt.Printf("Hello World\n")
	case "pl":
		// spell-checker: disable-next-line
		_, _ = fmt.Printf("Witaj świecie\n")
	}

	return nil
}

func (cmd *mainCmd) Run(ctx context.Context, args []string) error {
	// We _still_ need the Run function, but it is now just calling the
	// MakeCmd().Run() combo. The reasons for why this is not hidden will become
	// apparent in the next sample but suffice it to say that this is where we
	// can mix our FlagRunner with extensions to share code and behavior across
	// commands.
	//
	// It is worth noting that there's very little framework-mandated magic
	// going on. You are still in control: RunMain still calls this Run
	// function. The value returned by MakeCmd _does_ handle command line
	// argument parsing and error handling, but it does so in a way that is
	// virtually never going to be inappropriate, and is the sort of copy-pasted
	// code that you don't want to have to write over and over. The provided Run
	// function proceeds to call our RunFlag function defined above.
	//
	// If any of this does not fit your program, you can always adjust things
	// on a case-by-case basis. We've tried to design go-cmdr in a way that
	// doesn't force you to do things in one specific way that must then
	// (somehow) fit all the cases.
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func main() {
	cmdr.RunMain(&mainCmd{}, os.Args)
}
