// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"context"
	"flag"
	"fmt"
	"strings"
)

type LanguageExt struct {
	lang      string
	Supported []string
}

// Lang returns the selected language.
func (ext *LanguageExt) Lang() string {
	return ext.lang
}

func (ext *LanguageExt) ExtendFlag(ctx context.Context, fl *flag.FlagSet) context.Context {
	fl.StringVar(&ext.lang, "l", "en", fmt.Sprintf("Greeting Language (%s)", strings.Join(ext.Supported, ", ")))

	return ctx
}

func (ext *LanguageExt) RunFlag(context.Context, *flag.FlagSet) error {
	for _, l := range ext.Supported {
		if l == ext.lang {
			return nil
		}
	}

	return fmt.Errorf("unsupported language: %s", ext.lang)
}
