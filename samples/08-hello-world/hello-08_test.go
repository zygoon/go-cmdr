// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main_test

import (
	"testing"

	"gitlab.com/zygoon/go-cmdr/cmdtest"

	main "gitlab.com/zygoon/go-cmdr/samples/08-hello-world"
)

func TestMainCmd(t *testing.T) {
	t.Run("no-args", func(t *testing.T) {
		inv := cmdtest.Invoke(&main.Cmd{})
		if err := inv.ExpectStdout("Hello World\n"); err != nil {
			t.Error(err)
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Error(err)
		}

		if err := inv.ExpectExitCode(0); err != nil {
			t.Fatal(err)
		}
	})

	t.Run("English", func(t *testing.T) {
		inv := cmdtest.Invoke(&main.Cmd{}, "-l", "en")
		if err := inv.ExpectStdout("Hello World\n"); err != nil {
			t.Error(err)
		}
	})

	t.Run("Polish", func(t *testing.T) {
		inv := cmdtest.Invoke(&main.Cmd{}, "-l", "pl")
		if err := inv.ExpectStdout("Witaj świecie\n"); err != nil {
			t.Error(err)
		}
	})

	t.Run("German", func(t *testing.T) {
		inv := cmdtest.Invoke(&main.Cmd{}, "-l", "de")
		// Do you remember the bug from the previous example?
		// The bug is now gone and our test is here to prove it.
		if err := inv.ExpectStdout(""); err != nil {
			t.Error(err)
		}

		if err := inv.ExpectStderr("08-hello-world error: unsupported language: de\n"); err != nil {
			t.Error(err)
		}

		if err := inv.ExpectExitCode(1); err != nil {
			t.Fatal(err)
		}
	})
}
