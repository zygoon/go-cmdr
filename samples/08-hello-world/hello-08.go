// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"context"
	"flag"
	"os"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"gitlab.com/zygoon/go-cmdr/format"
	"gitlab.com/zygoon/go-cmdr/format/jsonformat"
)

type mainCmd struct {
	// The language extension is still here, we just moved it to another file.
	LanguageExt
	// The format extension allows us to provide output in one of several
	// formats. The base library contains utilities for plain text,
	// shell-compatible and JSON formats. Additional formats may be defined
	// by third party libraries or application programs.
	FormatExt format.Ext

	msg string
}

func (cmd *mainCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (cmd *mainCmd) RunFlag(ctx context.Context, _ *flag.FlagSet) error {
	// We get to do any computation we want. Here we just compute the message from the language
	// extension and save it into an instance variable.
	switch cmd.LanguageExt.Lang() {
	case "", "en":
		cmd.msg = "Hello World"
	case "pl":
		// spell-checker: disable-next-line
		cmd.msg = "Witaj świecie"
	}

	// To produce output we call NegotiateCallback on the format extension. This
	// is similar to format.NegotiateCallback, with the exception that all the
	// callback binding is done for us.
	return cmd.FormatExt.NegotiateCallback(ctx)
}

func (cmd *mainCmd) Run(ctx context.Context, args []string) error {
	cmd.LanguageExt.Supported = []string{"en", "pl"}
	// The format extension is initialized here.
	cmd.FormatExt = format.Ext{
		format.PlainText(cmd.printPlain),
		jsonformat.Callback(cmd.printJson),
	}

	// The command is assembled, combined with both extensions and started.
	return flagcmd.MakeCmd(cmd, &cmd.LanguageExt, &cmd.FormatExt).Run(ctx, args)
}

// printPlain prints the message.
func (cmd *mainCmd) printPlain(ctx context.Context, p *format.PlainPrinter) error {
	_, err := p.Println(cmd.msg)

	return err
}

// printJson prints the message as a JSON object.
func (cmd *mainCmd) printJson(ctx context.Context, p *jsonformat.Printer) error {
	return p.PrintEncoded(map[string]string{"message": cmd.msg})
}


func main() {
	cmdr.RunMain(&mainCmd{}, os.Args)
}
