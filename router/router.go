// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package router provides command router useful for creating arbitrary sub-command trees.
package router

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"path/filepath"
	"sort"
	"strings"
	"text/tabwriter"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"gitlab.com/zygoon/go-cmdr/internal"
)

// OneLiner is an interface of obtaining one-line command help messages.
type OneLiner interface {
	// OneLiner returns a help message when used inside Cmd.
	OneLiner() string
}

var (
	// ErrExpectedCmd reports that Cmd expected a command name but didn't find any.
	ErrExpectedCmd = errors.New("expected command name")

	// ErrUnknownCmd reports that Cmd was asked to run a command it doesn't know about.
	ErrUnknownCmd = errors.New("unknown command")
)

// Router is the deprecated name for the router command.
//
// Deprecated: To avoid stuttering router.Router, the type was renamed to
// router.Cmd.
type Router = Cmd

// Cmd routes execution to a named command.
//
// Cmd implements cmdr.Cmd interface, allowing arbitrary nesting of commands.
// Commands should implement the OneLiner interface for improved user
// experience.
//
// A tree of routers automatically keeps track of the command name. A router
// may explicitly set a custom name, which disables this mechanism.
type Cmd struct {
	Commands map[string]cmdr.Cmd
	Implicit cmdr.Cmd // Implicit command to run

	Name         string // for built-in usage, optional
	OneLinerText string // for built-in help when nested inside another router

	// UseArgv0 instructs the router to route based on the base name of args[0],
	// with the .exe extension trimmed. This allows packing multiple commands
	// into one multiplexer and dispatching based on symbolic link name.
	UseArgv0 bool
}

// PreserveArgs0 returns true if UseArgv0 is set.
func (c *Cmd) PreserveArgs0() bool {
	return c.UseArgv0
}

// OneLiner implements Helper returning OneLinerText.
func (c *Cmd) OneLiner() string {
	return c.OneLinerText
}

// FlagUsage prints the usage message of the router command.
//
// FlagUsage implements the flagcmd.Helper interface.
func (c *Cmd) FlagUsage(fl *flag.FlagSet) {
	w := fl.Output()

	hasOptions := false

	fl.VisitAll(func(f *flag.Flag) {
		hasOptions = true
	})

	switch {
	case c.Implicit == nil && hasOptions:
		_, _ = fmt.Fprintf(w, "Usage: %s [OPTIONS] COMMAND [...]\n", fl.Name())
	case c.Implicit != nil && hasOptions:
		_, _ = fmt.Fprintf(w, "Usage: %s [OPTIONS] [COMMAND] [...]\n", fl.Name())
	case c.Implicit == nil && !hasOptions:
		_, _ = fmt.Fprintf(w, "Usage: %s COMMAND [...]\n", fl.Name())
	case c.Implicit != nil && !hasOptions:
		_, _ = fmt.Fprintf(w, "Usage: %s [COMMAND] [...]\n", fl.Name())
	}

	if hasOptions {
		_, _ = fmt.Fprintf(w, "\nAvailable options:\n")

		fl.PrintDefaults()
	}

	if len(c.Commands) > 0 {
		_, _ = fmt.Fprintf(w, "\nAvailable commands:\n")

		// Use a tab writer for more appealing output.
		tw := tabwriter.NewWriter(w, 0, 4, 2, ' ', 0)
		defer func() {
			_ = tw.Flush()
		}()

		names := make([]string, 0, len(c.Commands))
		for name := range c.Commands {
			names = append(names, name)
		}

		sort.Strings(names)

		for _, name := range names {
			if cmd, ok := c.Commands[name].(OneLiner); ok {
				_, _ = fmt.Fprintf(tw, "\t%s\t%s\n", name, cmd.OneLiner())
			} else {
				_, _ = fmt.Fprintf(tw, "\t%s\t\n", name)
			}
		}
	}
}

// FlagSet returns a new, configured FlagSet.
func (c *Cmd) FlagSet(ctx context.Context) *flag.FlagSet {
	// routerName is retained for compatibility.
	routerName := c.Name
	if routerName == "" {
		routerName = cmdr.Name(ctx)
	}

	return flag.NewFlagSet(routerName, flag.ContinueOnError)
}

// RunFlag runs the command with the given context and parsed flag set.
//
// RunFlag pops the first argument and routes execution to another command.
//
// Command context is modified to carry the name of the outer command. Use
// OuterCmdName to retrieve it.
func (c *Cmd) RunFlag(ctx context.Context, fs *flag.FlagSet) error {
	var cmd cmdr.Cmd

	var cmdArgs []string

	routerName := c.Name
	if routerName == "" {
		routerName = cmdr.Name(ctx)
	}

	ctx = withOuterCmdName(ctx, routerName)

	if fs.NArg() > 0 {
		cmdName := fs.Arg(0)
		cmdArgs = fs.Args()[1:]

		if c.UseArgv0 {
			cmdName = strings.TrimSuffix(filepath.Base(cmdName), cmdr.ExeSuffix)
		} else {
			ctx = internal.WithName(ctx, routerName+" "+cmdName)
		}

		cmd = c.Commands[cmdName]
		if cmd == nil {
			fs.Usage()
			return ErrUnknownCmd
		}
	} else {
		cmd = c.Implicit
		cmdArgs = fs.Args()
	}

	if fs.NArg() == 0 && cmd == nil {
		fs.Usage()
		return ErrExpectedCmd
	}

	return cmd.Run(ctx, cmdArgs)
}

// Run pops the first argument and routes execution to another command.
//
// If you are using a flag-based argument parser and want to provide
// additional flags directly to the router, call flagcmd.MakeCmd with
// extensions that provide such flags.
//
// Command context is modified to carry the name of the outer command. Use
// OuterCmdName to retrieve it.
func (c *Cmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(c).Run(ctx, args)
}

type outerCmdKey struct{}

// withOuterCmdName returns a context remembering the outer command name.
func withOuterCmdName(parent context.Context, name string) context.Context {
	return context.WithValue(parent, outerCmdKey{}, name)
}

// OuterCmdName returns the outer command name.
//
// Deprecated: With the introduction of cmdr.Name, using OuterCmdName is not
// beneficial, as tracking the outer command name and sub-command name is more
// complex than just tracking the command name alone.
func OuterCmdName(ctx context.Context) (name string, ok bool) {
	name, ok = ctx.Value(outerCmdKey{}).(string)
	return name, ok
}
