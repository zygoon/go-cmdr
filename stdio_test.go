// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package cmdr_test

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"testing"

	"gitlab.com/zygoon/go-cmdr"
)

func TestRedirectingStdio(t *testing.T) {
	var bufStdin, bufStdout, bufStderr bytes.Buffer

	if _, err := bufStdin.WriteString("stdin"); err != nil {
		t.Fatalf("cannot write to stdin: %v", err)
	}

	ctx := cmdr.WithStdio(context.Background(), &bufStdin, &bufStdout, &bufStderr)
	stdin, stdout, stderr := cmdr.Stdio(ctx)

	stdinData, err := io.ReadAll(stdin)

	if err != nil {
		t.Fatalf("cannot read from stdin: %v", err)
	}

	if string(stdinData) != "stdin" {
		t.Fatalf("Stdin redirection failed")
	}

	if _, err := fmt.Fprintf(stdout, "stdout"); err != nil {
		t.Fatalf("cannot write to stdout: %v", err)
	}

	if s := bufStdout.String(); s != "stdout" {
		t.Fatalf("Stdout redirection failed")
	}

	if _, err := fmt.Fprintf(stderr, "stderr"); err != nil {
		t.Fatalf("cannot write to stderr: %v", err)
	}

	if s := bufStderr.String(); s != "stderr" {
		t.Fatalf("Stderr redirection failed, read: %v", s)
	}
}

func TestStdioDefaults(t *testing.T) {
	stdin, stdout, stderr := cmdr.Stdio(context.Background())

	if stdin == nil {
		t.Fatalf("Missing default value for stdin")
	}

	if stdout == nil {
		t.Fatalf("Missing default value for stdout")
	}

	if stderr == nil {
		t.Fatalf("Missing default value for stderr")
	}
}
