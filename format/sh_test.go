// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package format_test

import (
	"bytes"
	"testing"

	"gitlab.com/zygoon/go-cmdr/format"
)

func TestOutputFormat_ShellCompat(t *testing.T) {
	t.Setenv("OUTPUT_FORMAT", "sh")

	var buf bytes.Buffer

	somePrinter, err := format.Negotiate(&buf, nil, format.ShellCompatSupport)
	if err != nil {
		t.Fatal(err)
	}

	p := somePrinter.(*format.ShellPrinter)

	if err := p.PrintSetVar("FOO", "some value"); err != nil {
		t.Fatal(err)
	}

	if err := p.PrintExportVar("BAR", "other value"); err != nil {
		t.Fatal(err)
	}

	if err := p.PrintComment("This is safe\nEven if you have newlines"); err != nil {
		t.Fatal(err)
	}

	if v := buf.String(); v != "FOO=\"some value\"\nexport BAR=\"other value\"\n# This is safe\n# Even if you have newlines\n" {
		t.Fatalf("Unexpected output: %q", v)
	}
}
