// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package format

import (
	"context"
	"flag"
	"os"
	"strings"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/internal"
)

// envVar implements flag.Value bridge to a given environment variable.
type envVar string

func (v envVar) String() string {
	return os.Getenv(string(v))
}

func (v envVar) Set(s string) error {
	return os.Setenv(string(v), s)
}

// Ext is a flagcmd extension for selecting and executing format-specific output.
//
// Ext is meant to be used with flagcmd.MakeCmd.
type Ext []Callback

// ExtendFlag extends the flag set to provide the -output-format flag.
//
// The added flag is bridged to the OUTPUT_FORMAT environment variable.
// The help message is derived from the set of declared output formats.
func (ext Ext) ExtendFlag(ctx context.Context, fl *flag.FlagSet) context.Context {
	var sb strings.Builder

	_, _ = sb.WriteString("Select output format: ")

	for idx, cb := range ext {
		switch idx {
		case 0:
		case len(ext) - 1:
			_, _ = sb.WriteString(", or ")
		default:
			_, _ = sb.WriteString(", ")
		}

		switch f := cb.FormatName(); f {
		case "":
			// TODO: This will have to evolve to handle clashing bash completion output format.
			_, _ = sb.WriteString("empty for plain text")
		case shellFormatName:
			_, _ = sb.WriteString("sh (for shell)")
		default:
			_, _ = sb.WriteString(f)
		}
	}

	fl.Var(envVar(EnvVariable), "output-format", sb.String())

	return ctx
}

// RunFlag does nothing at all
func (ext Ext) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	return nil
}

// NegotiateCallback selects the output format and calls the printer function.
func (ext Ext) NegotiateCallback(ctx context.Context) error {
	_, stdout, _ := cmdr.Stdio(ctx)
	args := internal.Args(ctx)

	return NegotiateCallback(ctx, stdout, args, ext...)
}
