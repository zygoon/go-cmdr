// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package format

import (
	"context"
	"fmt"
	"io"
	"strings"
)

const shellFormatName = "sh"

// ShellCompatSupport indicates application support for shell-compatible output.
//
// Deprecated: Use Shell with NegotiateCallback for better type safety.
var ShellCompatSupport Support = &formatSupport{
	name: shellFormatName,
	printer: func(w io.Writer, _, props []string) any {
		return NewShellPrinter(w)
	},
}

// Shell  returns a callback for shell-compatible output.
func Shell(fn func(ctx context.Context, p *ShellPrinter) error) Callback {
	return &formatCallback[ShellPrinter]{
		name: shellFormatName,
		fn:   fn,
		makePrinter: func(w io.Writer, args, params []string) *ShellPrinter {
			return NewShellPrinter(w)
		},
	}
}

// ShellPrinter assists in producing shell-compatible output.
type ShellPrinter struct {
	w io.Writer
}

// NewShellPrinter returns a ShellPrinter using the given writer.
func NewShellPrinter(w io.Writer) *ShellPrinter {
	return &ShellPrinter{w: w}
}

// PrintComment prints a shell comment.
func (p *ShellPrinter) PrintComment(text string) error {
	_, err := fmt.Fprintf(p.w, "# %s\n", strings.ReplaceAll(text, "\n", "\n# "))

	return err
}

// PrintSetVar prints a variable assignment.
func (p *ShellPrinter) PrintSetVar(name, value string) error {
	_, err := fmt.Fprintf(p.w, "%s=%q\n", name, value)

	return err
}

// PrintExportVar prints an exported variable assignment.
func (p *ShellPrinter) PrintExportVar(name, value string) error {
	_, err := fmt.Fprintf(p.w, "export %s=%q\n", name, value)

	return err
}
