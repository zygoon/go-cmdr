// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// Package format implements the output format hint system.
//
// Compatible programs may use output hints to select the preferred output
// format. Typically, this is used to switch to a format that is friendly to
// parse, such as JSON or shell variable assignment.
//
// Format is selected by defining the environment variable OUTPUT_FORMAT= to a
// non-empty value. The value is a comma-separated list, of potentially just
// one element. The first word in the list selects the desired format.
// Well-known cases are documented below.
//
// The remaining words configure the particular properties of the format, such
// as pretty-printing JSON output for human readability. Using an unknown
// format name is an error. Unknown format properties are silently ignored.
//
// # Plain text format.
//
// - Usage: OUTPUT_FORMAT="" (empty string, default)
// - Format callback type: PlainText
//
// The default format is plain text. Applications are further encouraged to
// suppress use of color output, as defined by the quasi-standard variable
// NO_COLOR.
//
// # Shell-compatible variable assignment format.
//
// - Usage: OUTPUT_FORMAT=sh
// - Format callback type: Shell
//
// Output format of shell-compatible variable assignments. This is useful for
// usage in scripting, by eval-lading variables. Values should be quoted
// correctly. Any output that is not a key="value" variable assignment must use
// the shell comment syntax to avoid disrupting the parse/eval process.
//
// # JSON output format.
//
// - Usage: OUTPUT_FORMAT=json,[pretty]
// - Format callback type: jsonformat.Callback
//
// Output format of JSON structured data. This is ideal for parsing in more
// complex applications that have access to an off-the-shelf JSON library.
//
// - Parameter: pretty (boolean flag)
//
// Enables pretty-printed JSON output, suitable for humans. The
// default output is optimized for machines.
package format

import (
	"io"
	"os"
	"strings"
)

// Negotiate negotiates the output format of the application.
//
// Negotiate looks at the environment variable OUTPUT_FORMAT, and at the list of
// formats the application explicitly supports. Some formats may require a peek
// at the command line arguments. Selected output format is used to instantiate
// a printer with the given writer. The printer offers APIs for producing
// output compatible with the format. Applications must type-switch on the
// returned printer value to access them.
//
// Applications are encouraged to support one of the well-known formats, such
// as PlainTextSupport, ShellCompatSupport or jsonformat.Support. Applications are
// free to provide custom formats. Reasonable applications should support the
// default plain text format.
//
// If an unknown format is requested, the error is ErrUnknownFormat.
//
// Deprecated: use NegotiateCallback for better type safety. Each format.Support
// value needs to be replaced with format.Callback.
func Negotiate(w io.Writer, args []string, supported ...Support) (printer any, err error) {
	name, params := nameParams()
	highestPriority := -1

	for _, sup := range supported {
		if sup.FormatName() == name {
			var p int

			if sup, ok := sup.(PrioritySupport); ok {
				p = sup.FormatPriority(args)
			}

			if p > highestPriority {
				highestPriority = p
				printer = sup.FormatPrinter(w, args, params)
			}
		}
	}

	if printer == nil {
		return nil, ErrUnknownFormat
	}

	return printer, nil
}

// Support is an interface for declaring support for an output format.
type Support interface {
	// FormatName returns a name matched against OUTPUT_FORMAT.
	FormatName() string
	// FormatPrinter returns a configured printer specific for the format.
	FormatPrinter(w io.Writer, args, params []string) any
}

// PrioritySupport allows assigning varying priority levels to output formats.
type PrioritySupport interface {
	Support

	// FormatPriority returns the priority a given format should take.
	// Negative values indicate that the format should be ignored.
	FormatPriority(args []string) int
}

type formatSupport struct {
	name    string
	printer func(w io.Writer, args, params []string) any
}

func (supp *formatSupport) FormatName() string {
	return supp.name
}

func (supp *formatSupport) FormatPrinter(w io.Writer, args, params []string) any {
	return supp.printer(w, args, params)
}

var (
	// ErrUnknownFormat records usage of unknown OUTPUT_FORMAT.
	//
	// When surfaced as a process exit status, it becomes EX_DATAERR (65).
	ErrUnknownFormat = unknownFormatError{}
)

type unknownFormatError struct{}

func (unknownFormatError) Error() string {
	return "unknown output format"
}

// ExitCode returns the exit code used when an unknown format is requested.
func (unknownFormatError) ExitCode() int {
	return 65 // This is EX_DATAERR from sysexits.h
}

// EnvVariable is the name of the OUTPUT_FORMAT variable.
const EnvVariable = "OUTPUT_FORMAT"

func nameParams() (name string, params []string) {
	if parts := strings.Split(os.Getenv(EnvVariable), ","); len(parts) > 0 {
		return parts[0], parts[1:]
	}

	return "", nil
}
