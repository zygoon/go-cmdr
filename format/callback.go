// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package format

import (
	"context"
	"io"
)

// Callback is an interface for calling a callback for a given output format.
type Callback interface {
	// FormatName returns a name matched against OUTPUT_FORMAT.
	FormatName() string
	// FormatCallback creates a printer and calls the function consuming
	// it. The type-specific interface between the format printer and the
	// callback function is not exposed here.
	FormatCallback(ctx context.Context, w io.Writer, args, params []string) error
}

// PriorityCallback allows assigning varying priority levels to output formats
type PriorityCallback interface {
	Callback

	// FormatPriority returns the priority a given format should take.
	// Negative values indicate that the format should be ignored.
	FormatPriority(args []string) int
}

// NegotiateCallback negotiates the output format of the application.
//
// NegotiateCallback looks at the environment variable OUTPUT_FORMAT, and at
// the list of formats the application explicitly supports. Some formats may
// require a peek at the command line arguments. Selected output format is used
// to instantiate a printer with the given writer and to pass it to a
// printer-aware callback function. The printer offers APIs for producing
// output compatible with the format.
//
// Applications are encouraged to support one of the well-known formats, such
// as PlainText, Shell or jsonformat.Callback. Applications are free to provide
// custom formats. Reasonable applications should support the default plain
// text format.
//
// If an unknown format is requested, the error is ErrUnknownFormat.
func NegotiateCallback(ctx context.Context, w io.Writer, args []string, callbacks ...Callback) error {
	name, params := nameParams()
	highestPriority := -1

	var callback Callback

	for _, cb := range callbacks {
		if cb.FormatName() == name {
			var p int

			if sup, ok := cb.(PriorityCallback); ok {
				p = sup.FormatPriority(args)
			}

			if p > highestPriority {
				highestPriority = p
				callback = cb
			}
		}
	}

	if callback == nil {
		return ErrUnknownFormat
	}

	return callback.FormatCallback(ctx, w, args, params)
}

// formatCallback implements Callback for a specific printer type.
type formatCallback[Printer any] struct {
	name        string
	makePrinter func(w io.Writer, args, params []string) *Printer
	fn          func(ctx context.Context, p *Printer) error
}

// FormatName returns the name of the format.
func (cb *formatCallback[Printer]) FormatName() string {
	return cb.name
}

// FormatCallback makes a printer and calls the callback with it.
func (cb *formatCallback[Printer]) FormatCallback(ctx context.Context, w io.Writer, args, params []string) error {
	p := cb.makePrinter(w, args, params)

	return cb.fn(ctx, p)
}
