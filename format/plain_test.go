// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package format_test

import (
	"bytes"
	"io"
	"testing"

	"gitlab.com/zygoon/go-cmdr/format"
)

func TestOutputFormat_PlainText(t *testing.T) {
	t.Setenv("OUTPUT_FORMAT", "")

	var buf bytes.Buffer

	t.Run("no-color", func(t *testing.T) {
		t.Setenv("NO_COLOR", "1")
		buf.Reset()

		p := expectPlainText(t, &buf)

		if !p.NoColor {
			t.Fatalf("No color mode should have been honored")
		}

		_, _ = p.Println("potato")
		_, _ = p.Printf("more potatoes\n")

		if v := buf.String(); v != "potato\n"+"more potatoes\n" {
			t.Fatalf("Unexpected output: %q", v)
		}
	})

	t.Run("color", func(t *testing.T) {
		t.Setenv("NO_COLOR", "")
		buf.Reset()

		p := expectPlainText(t, &buf)

		if p.NoColor {
			t.Fatalf("Unexpected no-color mode")
		}

		_, _ = p.Println("potato")
		_, _ = p.Printf("more potatoes\n")

		if v := buf.String(); v != "potato\n"+"more potatoes\n" {
			t.Fatalf("Unexpected output: %q", v)
		}
	})
}

func expectPlainText(t *testing.T, w io.Writer) *format.PlainPrinter {
	t.Helper()

	of, err := format.Negotiate(w, nil, format.PlainTextSupport)
	if err != nil {
		t.Fatal(err)
	}

	return of.(*format.PlainPrinter)
}
