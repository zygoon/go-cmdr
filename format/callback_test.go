// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package format_test

import (
	"context"
	"errors"
	"io"
	"log"
	"os"
	"testing"

	"gitlab.com/zygoon/go-cmdr/format"
)

func ExampleNegotiateCallback() {
	_ = os.Setenv(format.EnvVariable, "sh")
	defer func() {
		_ = os.Unsetenv(format.EnvVariable)
	}()

	ctx := context.Background()

	err := format.NegotiateCallback(ctx, os.Stdout, nil,
		format.PlainText(func(ctx context.Context, p *format.PlainPrinter) error {
			_, err := p.Println("Hello World")

			return err
		}),
		format.Shell(func(ctx context.Context, p *format.ShellPrinter) error {
			return p.PrintSetVar("HELLO", "World")
		}),
	)
	if err != nil {
		log.Fatal(err)
	}
	// Output: HELLO="World"
}

func TestNegotiateCallback_UnknownFormat(t *testing.T) {
	t.Setenv("OUTPUT_FORMAT", "potato")

	ctx := context.Background()

	t.Run("negotiate", func(t *testing.T) {
		err := format.NegotiateCallback(ctx, io.Discard, nil)
		if err == nil {
			t.Fatal("Unexpected success with OUTPUT_FORMAT=potato")
		}

		if !errors.Is(err, format.ErrUnknownFormat) {
			t.Fatalf("Expected ErrUnknownFormat error, got %v", err)
		}

		if v := err.Error(); v != "unknown output format" {
			t.Fatalf("Unexpected error message: %q", v)
		}

		if v := err.(interface{ ExitCode() int }).ExitCode(); v != 65 {
			t.Fatalf("Unexpected exit code: %d", v)
		}
	})
}

type customCallback struct {
	priority int
	fn       func(context.Context, *customPrinter) error
}

func (customCallback) FormatName() string {
	return "custom"
}

func (cb customCallback) FormatCallback(ctx context.Context, w io.Writer, _, _ []string) error {
	return cb.fn(ctx, &customPrinter{w: w, priority: cb.priority})
}

func (cb customCallback) FormatPriority([]string) int {
	return cb.priority
}

func TestNegotiateCallback_Priorities(t *testing.T) {
	t.Setenv("OUTPUT_FORMAT", "custom")

	ctx := context.Background()

	t.Run("order 0, -1, +1", func(t *testing.T) {
		selectedPriority := -100
		fn := func(ctx context.Context, p *customPrinter) error {
			selectedPriority = p.priority

			return nil
		}
		err := format.NegotiateCallback(ctx, io.Discard, nil,
			customCallback{fn: fn},
			customCallback{fn: fn, priority: -1},
			customCallback{fn: fn, priority: 1})
		if err != nil {
			t.Fatal(err)
		}

		if selectedPriority != 1 {
			t.Fatalf("High-priority support not selected")
		}
	})

	t.Run("order -1, 0, +1", func(t *testing.T) {
		selectedPriority := -100
		fn := func(ctx context.Context, p *customPrinter) error {
			selectedPriority = p.priority

			return nil
		}
		err := format.NegotiateCallback(ctx, io.Discard, nil,
			customCallback{fn: fn, priority: -1},
			customCallback{fn: fn},
			customCallback{fn: fn, priority: 1})
		if err != nil {
			t.Fatal(err)
		}

		if selectedPriority != 1 {
			t.Fatalf("High-priority support not selected")
		}
	})

	t.Run("order -1, +1, 0", func(t *testing.T) {
		selectedPriority := -100
		fn := func(ctx context.Context, p *customPrinter) error {
			selectedPriority = p.priority

			return nil
		}
		err := format.NegotiateCallback(ctx, io.Discard, nil,
			customCallback{fn: fn, priority: -1},
			customCallback{fn: fn, priority: 1},
			customCallback{fn: fn})
		if err != nil {
			t.Fatal(err)
		}

		if selectedPriority != 1 {
			t.Fatalf("High-priority support not selected")
		}
	})
}
