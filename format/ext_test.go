// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package format_test

import (
	"context"
	"flag"
	"testing"

	"gitlab.com/zygoon/go-cmdr/cmdtest"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"gitlab.com/zygoon/go-cmdr/format"
)

func TestExt(t *testing.T) {
	fe := format.Ext{
		format.Shell(func(ctx context.Context, p *format.ShellPrinter) error {
			return p.PrintSetVar("HELLO", "World")
		}),
		format.PlainText(func(ctx context.Context, p *format.PlainPrinter) error {
			_, err := p.Println("Hello World")

			return err
		}),
	}
	r := flagcmd.RunnerFunc(func(ctx context.Context, fl *flag.FlagSet) error {
		return fe.NegotiateCallback(ctx)
	})
	cmd := flagcmd.MakeCmd(r, fe)

	t.Run("help", func(t *testing.T) {
		inv := cmdtest.Invoke(cmd, "--help")

		const help = `<IGNORE>
Usage: format
  -output-format value
    <TAB>Select output format: sh (for shell), or empty for plain text
`

		if err := inv.ExpectStderr(help); err != nil {
			t.Fatal(err)
		}
	})

	t.Run("plain", func(t *testing.T) {
		inv := cmdtest.Invoke(cmd)

		if err := inv.ExpectStdout("Hello World\n"); err != nil {
			t.Fatal(err)
		}
	})

	t.Run("shell", func(t *testing.T) {
		inv := cmdtest.Invoke(cmd, "-output-format=sh")

		if err := inv.ExpectStdout("HELLO=\"World\"\n"); err != nil {
			t.Fatal(err)
		}
	})
}
