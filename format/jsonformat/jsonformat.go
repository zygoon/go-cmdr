// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// Package jsonformat implements support for outputting JSON. It is separated
// from the format package to avoid pulling in the encoding/json and reflect
// packages into all format-aware applications.
//
// Call format.NegotiateCallback with jsonformat.Callback to output JSON
// representation of your application data.
package jsonformat

import (
	"context"
	"encoding/json"
	"io"

	"gitlab.com/zygoon/go-cmdr/format"
)

const formatName = "json"

// Callback returns a callback for JSON output.
func Callback(fn func(ctx context.Context, p *Printer) error) format.Callback {
	return callback(fn)
}

type callback func(ctx context.Context, p *Printer) error

func (callback) FormatName() string {
	return formatName
}

func (cb callback) FormatCallback(ctx context.Context, w io.Writer, args, params []string) error {
	return cb(ctx, NewPrinter(w, args, params))
}

// Support indicates application support for JSON output.
//
// The "pretty" property controls JSON pretty-printing. By default, the format
// is designed for machines for parse, producing minimal, valid output.
//
// Deprecated: use Callback instead.
var Support format.Support = support{}

type support struct{}

func (support) FormatName() string {
	return formatName
}

func (support) FormatPrinter(w io.Writer, args, param []string) any {
	return NewPrinter(w, args, param)
}

// Printer assists in producing valid JSON output.
type Printer struct {
	enc *json.Encoder
}

// NewPrinter returns Printer using the given writer.
//
// The "pretty" parameter enabled pretty-printed output suitable for humans.
func NewPrinter(w io.Writer, args, param []string) *Printer {
	enc := json.NewEncoder(w)

	for _, prop := range param {
		if prop == "pretty" {
			enc.SetIndent("", "  ")
		}
	}

	return &Printer{enc: enc}
}

// PrintEncoded prints the JSON encoding of an object.
func (p *Printer) PrintEncoded(v any) error {
	return p.enc.Encode(v)
}
