// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package jsonformat_test

import (
	"bytes"
	"context"
	"io"
	"os"
	"testing"

	"gitlab.com/zygoon/go-cmdr/format"
	"gitlab.com/zygoon/go-cmdr/format/jsonformat"
)

func ExampleCallback() {
	_ = os.Setenv("OUTPUT_FORMAT", "json")
	defer func() { _ = os.Unsetenv("OUTPUT_FORMAT") }()

	ctx := context.Background()

	_ = format.NegotiateCallback(ctx, os.Stdout, nil, jsonformat.Callback(func(ctx context.Context, p *jsonformat.Printer) error {
		return p.PrintEncoded("Hello World")
	}))

	// Output: "Hello World"
}

func ExampleSupport() {
	_ = os.Setenv("OUTPUT_FORMAT", "json")
	defer func() { _ = os.Unsetenv("OUTPUT_FORMAT") }()

	//lint:ignore SA1019 testing deprecated interface.
	p, err := format.Negotiate(os.Stdout, nil, jsonformat.Support)
	if err != nil {
		panic(err)
	}

	switch p := p.(type) {
	case *jsonformat.Printer:
		_ = p.PrintEncoded("Hello World")
	}
	// Output: "Hello World"
}

func TestOutputFormat_Json(t *testing.T) {
	var buf bytes.Buffer

	t.Run("default", func(t *testing.T) {
		t.Setenv("OUTPUT_FORMAT", "json")
		buf.Reset()

		p := expectJSON(t, &buf)

		err := p.PrintEncoded(struct {
			Foo string `json:"foo"`
		}{
			Foo: "some value",
		})

		if err != nil {
			t.Fatal(err)
		}

		if v := buf.String(); v != "{\"foo\":\"some value\"}\n" {
			t.Fatalf("Unexpected output: %q", v)
		}
	})

	t.Run("pretty", func(t *testing.T) {
		t.Setenv("OUTPUT_FORMAT", "json,pretty")
		buf.Reset()

		p := expectJSON(t, &buf)

		err := p.PrintEncoded(struct {
			Foo string `json:"foo"`
		}{
			Foo: "some value",
		})

		if err != nil {
			t.Fatal(err)
		}

		if v := buf.String(); v != "{\n  \"foo\": \"some value\"\n}\n" {
			t.Fatalf("Unexpected output: %q", v)
		}
	})
}

func expectJSON(t *testing.T, w io.Writer) (p *jsonformat.Printer) {
	t.Helper()

	ctx := context.Background()
	err := format.NegotiateCallback(ctx, w, nil, jsonformat.Callback(func(ctx context.Context, pp *jsonformat.Printer) error {
		p = pp

		return nil
	}))

	if err != nil {
		t.Fatal(err)
	}

	return p
}
