// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package format

import (
	"context"
	"fmt"
	"io"
	"os"
)

// PlainTextSupport indicates application support for plain-text output.
//
// Deprecated: use PlainText with NegotiateCallback for better type safety.
var PlainTextSupport Support = &formatSupport{
	printer: func(w io.Writer, args, params []string) any {
		return NewPlainPrinter(w, args, params)
	},
}

// PlainText returns a callback for plain text output.
func PlainText(fn func(ctx context.Context, p *PlainPrinter) error) Callback {
	return &formatCallback[PlainPrinter]{
		name:        "",
		fn:          fn,
		makePrinter: NewPlainPrinter,
	}
}

// PlainPrinter assists in printing plain text.
type PlainPrinter struct {
	w io.Writer

	// NoColor implements the https://no-color.org/ quasi-standard.
	//
	// When NO_COLOR is a non-empty value, the application should refrain
	// from using ANSI escape codes that make the output more colorful.
	NoColor bool
}

// NewPlainPrinter returns a PlainPrinter with NoColor derived from the NO_COLOR environment variable.
func NewPlainPrinter(w io.Writer, args, params []string) *PlainPrinter {
	return &PlainPrinter{w: w, NoColor: os.Getenv("NO_COLOR") != ""}
}

// Printf prints formatted output.
func (p *PlainPrinter) Printf(format string, a ...any) (n int, err error) {
	// TODO: we could filter out format sequences related to color changes here.
	return fmt.Fprintf(p.w, format, a...)
}

// Println prints values followed by newline.
func (p *PlainPrinter) Println(a ...any) (n int, err error) {
	// TODO: we could filter out format sequences related to color changes here.
	return fmt.Fprintln(p.w, a...)
}
