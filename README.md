<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# About the Go "commander" Cmdr module

This module has grown out of common parts of various projects. In all cases, it
was desired to offer a consistent command-line experience. In some cases, it was
also desired to support sub-commands or to make the main executable a
multiplexer which acts based on argv[0].

The package is designed to be minimalistic but extensible. Anything which takes
a context and a slice of string can be invoked as a command.

## Overview of packages

The `cmdr` package provides the `Cmd` interface, the convenience type `Func`
as well as a function to call from `main`, namely `RunMain`. See the
documentation for a lot more details.

The `cmdr/router` package provides a command that routes execution to one of
any number of named commands. The router cooperates with `RunMain` allowing
command multiplexers, packing many commands efficiently into one executable.

The `cmdr/cmdtest` package provides utilities for testing command line tools,
with a way to capture and observe exit code and printed output and error
messages.

The `cmdr/format` package provides support for the `OUTPUT_FORMAT=`
quasi-standard, which allows applications to produce output in a format desired
by the user. This is excellent for producing JSON or shell-compatible output
for scripting and automation.

## Contributions

Contributions are welcome. Please try to respect Go requirements (1.18 at the
moment) and the overall coding and testing style.

## License and REUSE

This project is licensed under the Apache 2.0 license, see the `LICENSE` file
for details. The project is compliant with https://reuse.software/, making
it easy to ensure license and copyright compliance by automating software
bill-of-materials. In other words, it's a good citizen in the modern free
software stacks.
