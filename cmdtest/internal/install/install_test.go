// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package install_test

import (
	"testing"

	"gitlab.com/zygoon/go-cmdr/cmdtest"
	"gitlab.com/zygoon/go-cmdr/cmdtest/internal/install"
)

func TestCmd(t *testing.T) {
	inv := cmdtest.Invoke(install.Cmd{}, "--help")

	if err := inv.ExpectStderr("Usage of install:\n"); err != nil {
		t.Fatal(err)
	}
}

func TestCmdPtr(t *testing.T) {
	inv := cmdtest.Invoke(&install.Cmd{}, "--help")

	if err := inv.ExpectStderr("Usage of install:\n"); err != nil {
		t.Fatal(err)
	}
}
