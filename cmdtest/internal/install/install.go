// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// Package install exists to provide regression tests against
// https://gitlab.com/zygoon/go-cmdr/-/issues/4
package install

import (
	"context"
	"flag"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
)

type Cmd struct{}

func (c Cmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(c).Run(ctx, args)
}

func (Cmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	return nil
}

func (Cmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}
