// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package cmdtest_test

import (
	"bytes"
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"testing"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/cmdtest"
)

func TestExpectedStdout(t *testing.T) {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		_, stdout, _ := cmdr.Stdio(ctx)

		_, _ = fmt.Fprintln(stdout, "Hello World")

		return nil
	})

	inv := cmdtest.Invoke(cmd)

	if err := inv.ExpectStdout("Hello World\n"); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStdout("Hello world\n"); err == nil {
		t.Fatalf("expected capitalization mismatch")
	} else {
		const expectedMsg = `unexpected stdout content
--- (expected)
+++ ( actual )
@@ -1 +1 @@
-Hello world
+Hello World
`
		if err.Error() != expectedMsg {
			t.Fatalf("unexpected message: %q", err.Error())
		}
	}
}

func TestExpectedStderr(t *testing.T) {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		_, _, stderr := cmdr.Stdio(ctx)

		_, _ = fmt.Fprintln(stderr, "Serious Warning")

		return nil
	})

	inv := cmdtest.Invoke(cmd)

	if err := inv.ExpectStderr("Serious Warning\n"); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStderr("Serious Error\n"); err == nil {
		t.Fatalf("expected wording mismatch")
	} else {
		const expectedMsg = `unexpected stderr content
--- (expected)
+++ ( actual )
@@ -1 +1 @@
-Serious Error
+Serious Warning
`
		if err.Error() != expectedMsg {
			t.Fatalf("unexpected message: %q", err.Error())
		}
	}
}

func TestExpectedExitCode(t *testing.T) {
	inv := cmdtest.Invoke(cmdr.Func(func(ctx context.Context, args []string) error {
		return nil
	}))

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}

	inv = cmdtest.Invoke(cmdr.Func(func(ctx context.Context, args []string) error {
		return errors.New("boom")
	}))

	if err := inv.ExpectExitCode(1); err != nil {
		t.Fatal(err)
	}

	inv = cmdtest.Invoke(cmdr.Func(func(ctx context.Context, args []string) error {
		return flag.ErrHelp
	}))

	if err := inv.ExpectExitCode(64); err != nil {
		t.Fatal(err)
	}

	inv = cmdtest.Invoke(cmdr.Func(func(ctx context.Context, args []string) error {
		return nil
	}))

	if err := inv.ExpectExitCode(42); err == nil {
		t.Fatalf("expected exit code mismatch")
	} else {
		const expectedMsg = "unexpected exit code: 0"

		if err.Error() != expectedMsg {
			t.Fatalf("unexpected message: %q", err.Error())
		}
	}
}

func TestCannedOutput(t *testing.T) {
	_, _, ok := cmdtest.CannedOutput("Potato").MatchesBuffer(bytes.NewBuffer([]byte("Potato")))

	if !ok {
		t.Fatal("CannedOutput did not match identical text")
	}
}

func TestTabEscaping(t *testing.T) {
	var panicArg interface{}

	func() {
		defer func() {
			panicArg = recover()
		}()

		_, _, _ = cmdtest.CannedOutput("\t").MatchesBuffer(bytes.NewBuffer([]byte("Potato")))
	}()

	if panicArg == nil {
		t.Fatal("expected test helper to panic")
	}

	if panicArg.(string) != "CannedOutput must not contain tab characters, use <TAB> to represent them." {
		t.Fatalf("unexpected panic argument: %s", panicArg)
	}
}

func TestTrailingSpaceEscaping(t *testing.T) {
	var panicArg interface{}

	func() {
		defer func() {
			panicArg = recover()
		}()

		_, _, _ = cmdtest.CannedOutput(" \n").MatchesBuffer(bytes.NewBuffer([]byte("Potato")))
	}()

	if panicArg == nil {
		t.Fatal("expected test helper to panic")
	}

	if panicArg.(string) != "CannedOutput must not contain trailing spaces, use <EOL> to disambiguate them." {
		t.Fatalf("unexpected panic argument: %s", panicArg)
	}
}

func TestLeadingNewlineEscaping(t *testing.T) {
	_, _, ok := cmdtest.CannedOutput("<IGNORE>\nPotato").MatchesBuffer(bytes.NewBuffer([]byte("Potato")))

	if !ok {
		t.Fatal("CannedOutput did not match text with leading <IGNORE>\\n")
	}
}

func TestCannedStdin(t *testing.T) {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		stdin, stdout, _ := cmdr.Stdio(ctx)

		data, err := io.ReadAll(stdin)
		if err != nil {
			return err
		}

		_, _ = fmt.Fprintf(stdout, "Stdin: %q\n", string(data))

		return nil
	})

	inv := cmdtest.Prepare(cmd)

	if _, err := inv.Stdin.WriteString("potato"); err != nil {
		t.Fatal(err)
	}

	if err := inv.RunPrepared(); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStdout("Stdin: \"potato\"\n"); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStderr(""); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}
}

func TestReplaceOption(t *testing.T) {
	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		_, stdout, _ := cmdr.Stdio(ctx)

		for i := 0; i < 1_000_000; i++ {
			_, _ = fmt.Fprintf(stdout, "This is quite a bit of stuff to write one million times")
		}

		return nil
	})

	inv := cmdtest.Prepare(cmd)
	inv.ReplaceOption(cmdtest.OptionBaseContext,
		cmdr.WithBaseContext(cmdr.WithStdio(context.Background(), &inv.Stdin, io.Discard, io.Discard)))

	if err := inv.RunPrepared(); err != nil {
		t.Fatal(err)
	}

	// Stdout buffer is empty, as we have redirected stdout to io.Discard.
	if err := inv.ExpectStdout(""); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStderr(""); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}
}

func TestExtraEnv(t *testing.T) {
	const varOutputFormat = "OUTPUT_FORMAT"

	t.Setenv(varOutputFormat, "potato")

	cmd := cmdr.Func(func(ctx context.Context, args []string) error {
		_, stdout, _ := cmdr.Stdio(ctx)

		_, _ = fmt.Fprintf(stdout, "%s=%s\n", varOutputFormat, os.Getenv(varOutputFormat))

		return nil
	})

	oldFormat := os.Getenv(varOutputFormat)

	inv := cmdtest.Prepare(cmd)
	inv.ExtraEnv = append(inv.ExtraEnv, fmt.Sprintf("%s=json,pretty", varOutputFormat))

	if err := inv.RunPrepared(); err != nil {
		t.Fatal(err)
	}

	if v := os.Getenv(varOutputFormat); v != oldFormat {
		t.Fatalf("%s is %q, expected %q", varOutputFormat, v, oldFormat)
	}

	if err := inv.ExpectStdout("OUTPUT_FORMAT=json,pretty\n"); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectStderr(""); err != nil {
		t.Fatal(err)
	}

	if err := inv.ExpectExitCode(0); err != nil {
		t.Fatal(err)
	}
}
