// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

// Package cmdtest contains utilities for testing commands.
package cmdtest

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"sync"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-diff"
)

const (
	msgExpected = "(expected)"
	msgActual   = "( actual )"
)

// Invocation represents a single execution of a command for testing.
type Invocation struct {
	Cmd                   cmdr.Cmd
	Args                  []string
	ExtraEnv              []string // Additional environment variables.
	Stdin, Stdout, Stderr bytes.Buffer
	ExitCode              int
	Options               []cmdr.Option
	invoked               bool
}

// Prepare returns an Invocation for the given command and arguments.
//
// Unlike Invoke, Prepare returns an Invocation that can be modified before
// being used with Invocation.RunPrepared.
//
// The returned Invocation has a number of options, like those that can be
// passed to RunMain.  See OptionIndex for a list of those options. The options
// may be either entirely replaced or selectively modified with
// Invocation.ReplaceOption.
func Prepare(cmd cmdr.Cmd, args ...string) *Invocation {
	if len(os.Args) == 0 {
		panic("os.Args must have at least one argument")
	}

	argv0 := filepath.Base(os.Args[0])
	argv0 = strings.TrimSuffix(strings.TrimSuffix(argv0, cmdr.ExeSuffix), ".test")

	if runtime.GOOS == "windows" && argv0 == "test" {
		// On Windows, Go may choose to rename the test executable to "test.test.exe" in
		// order to avoid special behavior caused by Windows for programs with certain names,
		// that historically required administrative privileges.
		//
		// This causes portability issues for tests that emit the name of the tested package
		// as the default argv0 value. This is especially pronounced in tests that observe
		// the help message or the usage string.
		//
		// In that particular case, use the name of the package as a fake argv0.
		cmdT := reflect.TypeOf(cmd)
		if cmdT.Kind() == reflect.Pointer {
			cmdT = cmdT.Elem()
		}

		argv0 = path.Base(cmdT.PkgPath())
	}

	return PrepareWithArgv0(cmd, argv0, args...)
}

// OptionIndex is an index to an option implicitly set up by Prepare.
type OptionIndex int

// Known values of OptionIndex.
const (
	// OptionArgv0 corresponds to option `cmdr.WithArgv0(argv0)`.
	OptionArgv0 OptionIndex = 0
	// OptionExitFunc corresponds to option `cmdr.WithExitFunc(func(code int) {
	// inv.ExitCode = code })`.
	//
	// This option implements the mechanism that captures the exit code from
	// a command.
	OptionExitFunc OptionIndex = 1
	// OptionPanicBehavior corresponds to option
	// `cmdr.WithPanicBehavior(cmdr.RecoverTraceExit)`.
	OptionPanicBehavior OptionIndex = 2
	// OptionBaseContext corresponds to option
	// `cmdr.WithBaseContext(cmdr.WithStdio(context.Background(), &inv.Stdin,
	// &inv.Stdout, &inv.Stderr))`.
	//
	// This option implements the mechanism that redirects standard I/O
	// streams.
	OptionBaseContext OptionIndex = 3
)

// ReplaceOption replaces one of the default invocation options.
//
// The main intent for this function is to allow test code to use Prepare and
// then replace the base context with one that uses a custom reader or writer
// for the I/O streams.
//
// The function panics if the index is out of range.
func (inv *Invocation) ReplaceOption(idx OptionIndex, op cmdr.Option) {
	if idx < 0 || int(idx) >= len(inv.Options) {
		panic("ReplaceOption cannot replace options outside of default range")
	}

	inv.Options[idx] = op
}

// PrepareWithArgv0 is like Prepare with explicit argv0.
func PrepareWithArgv0(cmd cmdr.Cmd, argv0 string, args ...string) *Invocation {
	a := make([]string, 0, len(args)+1)
	a = append(a, argv0)
	a = append(a, args...)

	inv := &Invocation{
		Cmd:  cmd,
		Args: a,
	}

	inv.Options = []cmdr.Option{
		cmdr.WithArgv0(argv0),
		cmdr.WithExitFunc(func(code int) { inv.ExitCode = code }),
		cmdr.WithPanicBehavior(cmdr.RecoverTraceExit),
		cmdr.WithBaseContext(cmdr.WithStdio(context.Background(), &inv.Stdin, &inv.Stdout, &inv.Stderr)),
	}

	return inv
}

// Invoke returns an Invocation for the given command and arguments.
//
// The tested command will be invoked with argv0 matching the name of the
// package being tested. Use InvokeWithArgv0 to provide a custom value.
//
// Tested command is passed to cmdr.RunMain for execution. Instead of exiting,
// non-zero exit code is recorded for inspection. Byte buffers replace standard
// input-output streams.
//
// Note that IO redirection does not happen at the OS level. Tested commands
// must use cmdr.Stdio helper function to access suggested IO interfaces.
func Invoke(cmd cmdr.Cmd, args ...string) *Invocation {
	inv := Prepare(cmd, args...)

	if err := inv.RunPrepared(); err != nil {
		panic(err)
	}

	return inv
}

// InvokeWithArgv0 is like Invoke with explicit argv0.
func InvokeWithArgv0(cmd cmdr.Cmd, argv0 string, args ...string) *Invocation {
	inv := PrepareWithArgv0(cmd, argv0, args...)

	if err := inv.RunPrepared(); err != nil {
		panic(err)
	}

	return inv
}

// envMutex protects from concurrent modification of the environment inside RunPrepared.
var envMutex sync.Mutex

// Run runs a prepared invocation.
func (inv *Invocation) RunPrepared() error {
	if inv.invoked {
		panic("RunPrepared can be called on result of Prepare or PrepareWithArgv0")
	}

	inv.invoked = true

	envMutex.Lock()
	defer envMutex.Unlock()

	oldEnv := os.Environ()

	if err := setenvMany(inv.ExtraEnv); err != nil {
		return fmt.Errorf("cannot modify environment: %w", err)
	}

	cmdr.RunMain(inv.Cmd, inv.Args, inv.Options...)

	os.Clearenv()

	if err := setenvMany(oldEnv); err != nil {
		return fmt.Errorf("cannot restore environment: %w", err)
	}

	return nil
}

func setenvMany(env []string) error {
	for _, kv := range env {
		k, v, err := splitEnv(kv)
		if err != nil {
			return err
		}

		if err := os.Setenv(k, v); err != nil {
			return err
		}
	}

	return nil
}

func splitEnv(ent string) (key, value string, err error) {
	bias := 0

	idx := strings.IndexRune(ent, '=')
	if idx == 0 {
		idx = strings.IndexRune(ent[1:], '=')
		bias = 1
	}

	if idx < 0 {
		return "", "", fmt.Errorf("malformed environment entry: %q", ent)
	}

	idx += bias

	return ent[:idx], ent[idx+1:], nil
}

// ExpectExitCode checks an expected exit code.
func (inv *Invocation) ExpectExitCode(code int) error {
	if v := inv.ExitCode; v != code {
		return fmt.Errorf("unexpected exit code: %d", v)
	}

	return nil
}

// ExpectStdout asserts the presence of expected content printed to stdout.
func (inv *Invocation) ExpectStdout(c CannedOutput) error {
	if a, e, ok := c.MatchesBuffer(&inv.Stdout); !ok {
		u, err := diff.ToUnified(msgExpected, msgActual, e, diff.Strings(e, a))
		if err != nil {
			return err
		}

		return fmt.Errorf("unexpected stdout content\n%s", u)
	}

	return nil
}

// ExpectStderr assets presence of expected content printed to stderr.
func (inv *Invocation) ExpectStderr(c CannedOutput) error {
	if a, e, ok := c.MatchesBuffer(&inv.Stderr); !ok {
		u, err := diff.ToUnified(msgExpected, msgActual, e, diff.Strings(e, a))
		if err != nil {
			return err
		}

		return fmt.Errorf("unexpected stderr content\n%s", u)
	}

	return nil
}

// CannedOutput represents the expected output of a command.
//
// This type automatically accepts conversions from string literals but makes
// it harder to misuse with variables of the string type.
type CannedOutput string

// MatchesBuffer returns actual, expected strings and equality flag
//
// The contents of the buffer is transformed, such, so that white-space
// differences become more apparent. Tabs present in the buffer are replaced
// with <TAB>, while any line-trailing white-space is terminated with <EOL>.
//
// In addition, leading <IGNORE> followed by the newline is stripped, so that
// backtick strings may be used more naturally.
func (c CannedOutput) MatchesBuffer(buf *bytes.Buffer) (a, e string, ok bool) {
	if bytes.ContainsRune([]byte(c), '\t') {
		panic("CannedOutput must not contain tab characters, use <TAB> to represent them.")
	}

	if bytes.Contains([]byte(c), []byte{' ', '\n'}) {
		panic("CannedOutput must not contain trailing spaces, use <EOL> to disambiguate them.")
	}

	tmp := buf.Bytes()
	// This avoids confusion related to tabs and spaces.
	tmp = bytes.ReplaceAll(tmp, []byte("\t"), []byte("<TAB>"))
	// This avoids confusion related to trailing spaces.
	tmp = bytes.ReplaceAll(tmp, []byte(" \n"), []byte(" <EOL>\n"))

	a = string(tmp)
	e = string(c)

	// This allows the removal of leading newline that aids in formatting.
	e = strings.TrimPrefix(e, "<IGNORE>\n")

	return a, e, a == e
}
